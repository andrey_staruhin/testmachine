﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Repositories;
using TestMachine.EfImplementation.Entities;

namespace TestMachine.EfImplementation
{
    public class EfTestRepository : ITestRepository
    {

        TestMachineContext _dbContext;

        public EfTestRepository(DbContextOptions<TestMachineContext> options)
        {
            _dbContext = new TestMachineContext(options);
        }


        public ITmTest AddTest(ITmTest test)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ITmTest> GetCompletedTestsForUser(object userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ITmTest> GetTests()
        {
            var tests = _dbContext.Tests.Include(test => test.EfQuestions).ToList();

            foreach(var test in tests)
            {
                foreach(var question in test.EfQuestions)
                {
                    _dbContext.Entry(question).Collection(q => q.EfQuestionOptions).Load();
                    _dbContext.Entry(question).Reference(q => q.EfTest).Load();
                }
            }

            return tests;


        }
    }
}
