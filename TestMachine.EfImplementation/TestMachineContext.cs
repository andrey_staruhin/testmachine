﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TestMachine.EfImplementation.Entities;

namespace TestMachine.EfImplementation
{
    public class TestMachineContext: DbContext
    {

        public TestMachineContext(DbContextOptions<TestMachineContext> options) : base(options)
        {

            
        }


        public DbSet<EfTest> Tests { set; get; }

        public DbSet<EfQuestion> Questions { set; get; }

        public DbSet<EfQuestionOption> QuestionOptions { set; get; }

        public DbSet<EfTestResult> TestResults { set; get; }

        public DbSet<EfTestAnswer> TestAnswers { set; get; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EfTest>().ToTable("Test");
            modelBuilder.Entity<EfQuestion>().ToTable("Question");
            modelBuilder.Entity<EfQuestionOption>().ToTable("Option");
            modelBuilder.Entity<EfTestResult>().ToTable("TestResult");
            modelBuilder.Entity<EfTestAnswer>().ToTable("Answer");



            modelBuilder.Entity<EfQuestion>().HasOne(q => q.EfTest).WithMany(test => test.EfQuestions).HasForeignKey(q => q.TestId);

            modelBuilder.Entity<EfQuestionOption>().HasOne(qo => qo.efQuestion).WithMany(q => q.EfQuestionOptions).HasForeignKey(qo => qo.QuestionId);
            modelBuilder.Entity<EfTestAnswer>().HasOne(ta => ta.TestResult).WithMany(tr => tr.Answers).HasForeignKey(ta => ta.TestResultId);

        }


        }
}
