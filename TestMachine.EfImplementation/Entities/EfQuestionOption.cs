﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.EfImplementation.Entities
{
    public class EfQuestionOption : ITmQuestionOption
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public bool IsCorrect { get; set; }

        public int QuestionId { set; get; }

        public EfQuestion efQuestion { get; set; }

    }
}
