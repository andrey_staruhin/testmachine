﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.EfImplementation.Entities
{
    public class EfTest : ITmTest
    {

        

        [Column("ID")]
        public int Id { get; set; }

        
        public string Name { get; set; }


        public IEnumerable<EfQuestion> EfQuestions { set; get; }



        public void AddQuestion(ITmQuestion question)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ITmQuestion> GetQuestions()
        {
            return EfQuestions;
        }
    }
}
