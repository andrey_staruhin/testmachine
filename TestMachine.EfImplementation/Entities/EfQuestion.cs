﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;

namespace TestMachine.EfImplementation.Entities
{
    public class EfQuestion : ITmQuestion
    {
        public int Id { get; set; }

        public int TestId { set; get; }

        public ITmTest Test { get; }

        [ForeignKey("TestId")]
        public EfTest EfTest { set; get; }

        public TmQuestionType QuestionType { get; }


        public IEnumerable<EfQuestionOption> EfQuestionOptions { set; get; }

        public string Text { get; set; }

        public void AddOption(ITmQuestionOption option)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ITmQuestionOption> GetOptions()
        {
            return EfQuestionOptions;
        }
    }
}
