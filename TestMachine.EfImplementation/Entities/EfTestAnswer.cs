﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TestMachine.EfImplementation.Entities
{
    public class EfTestAnswer
    {
        public int Id { set; get; }

        [ForeignKey("TestResultId")]
        public  EfTestResult TestResult { set; get; }

        public int TestResultId { set; get; }

        public int QuestionId { set; get; }

        public int OptionId { set; get; }

    }
}
