﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.EfImplementation.Entities
{
    public class EfTestResult : ITmTestResult
    {
        ITmTest _testForEvaluation;

     

        public EfTestResult(ITmTest test)
        {
            _testForEvaluation = test;
            TestId = test.Id;
            Answers = new List<EfTestAnswer>();
        }

        public EfTestResult()
        {
        }

        public int Id { get; set; }

        [NotMapped]
        public object UserId { get { return EfUserID; } set { EfUserID = (string)value; } }

        [Column("UserId")]
        public string EfUserID { get; set; }

        public List<EfTestAnswer> Answers { set; get; }

        public int TestId { set; get; }

        public void AddQuestionResult(int questiomId, List<int> optionIdList)
        {
            foreach(var optionId in optionIdList)
            {
                Answers.Add(new EfTestAnswer() { QuestionId = questiomId, OptionId = optionId, TestResultId = Id });
            }

            
        }

        private TmAnswer FromEfAnswersToDomainEntity(IEnumerable<EfTestAnswer> efAnswers)
        {
            TmAnswer tmAnswer = new TmAnswer() { ActualAnswers = efAnswers.Select(ef => ef.OptionId), Question = new EfQuestion() { Id = efAnswers.Select(ef => ef.QuestionId).First() } };

            return tmAnswer;
        }
        public IEnumerable<TmAnswer> GetAnswers()
        {

            return Answers.Select(answ => answ.QuestionId).Distinct()
                .Select(qId => Answers
                .Where(an => an.QuestionId == qId))
                .Select(efAnswers => FromEfAnswersToDomainEntity(efAnswers));
        }

        public float GetScore()
        {
            throw new NotImplementedException();
        }
    }
}
