﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Repositories;
using TestMachine.EfImplementation.Entities;

namespace TestMachine.EfImplementation
{
    public class EfTestResultRepository : ITestResultRepository
    {


        TestMachineContext _dbContext;

        public EfTestResultRepository(DbContextOptions<TestMachineContext> options)
        {
            _dbContext = new TestMachineContext(options);
        }

        public ITmTestResult AddTestResult(ITmTestResult testResult)
        {
           var result = _dbContext.TestResults.Add((EfTestResult)testResult).Entity;

            _dbContext.SaveChanges();

            return result;
        }

        public IEnumerable<ITmTestResult> GetTestResults(object userId)
        {
            return _dbContext.TestResults.Include(tr => tr.Answers).Where(tr=>(string)tr.UserId ==(string)userId).ToList();
        }
    }
}
