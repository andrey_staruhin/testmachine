﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;
using TestMachine.Domain.Factories;
using TestMachine.Domain.Repositories;
using TestMachine.EfImplementation.Entities;

namespace TestMachine.EfImplementation.Factories
{
    public class EfTmFactory : ITmFactory
    {
        DbContextOptions<TestMachineContext> _options;
        public EfTmFactory(DbContextOptions<TestMachineContext> options)
        {
            _options = options;
        }

        public ITmQuestion CreateQuestion(TmQuestionType questionType, string text)
        {
            throw new NotImplementedException();
        }

        public ITmQuestionOption CreateQuestionOption(string text, bool isCorrect)
        {
            throw new NotImplementedException();
        }

        public ITmTest CreateTest(string name)
        {
            throw new NotImplementedException();
        }

        public ITmTestResult CreateTestResult(object userId, int testId)
        {
            return new EfTestResult(GetTestRepository().GetTests().First(tst => tst.Id == testId)) { UserId = userId };
        }

        public ITestRepository GetTestRepository()
        {
            return new EfTestRepository(_options);
        }

        public ITestResultRepository GetTestResultRepository()
        {
            return new EfTestResultRepository(_options);
        }
    }
}
