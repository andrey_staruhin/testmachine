﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.FakeImplementation.Entities
{
    public class FakeTest : ITmTest
    {
        List<ITmQuestion> _questions = new List<ITmQuestion>();
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<ITmQuestion> GetQuestions()
        {
            return _questions;
        }

        public void AddQuestion(ITmQuestion question)
        {
            question.Id = _questions.Count + 1;
            _questions.Add(question);
        }
    }
}
