﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.FakeImplementation.Entities
{
    public class FakeTestResult : ITmTestResult
    {

        ITmTest _testForEvaluation;

        List<TmAnswer> _answers = new List<TmAnswer>();


        public FakeTestResult(ITmTest test)
        {
            _testForEvaluation = test;
            TestId = test.Id;
        }

        Dictionary<int, bool> answerResults = new Dictionary<int, bool>();
        public int Id { set; get; }
        
        public object UserId { get; set; }

        public int TestId { get; set; }

        public IEnumerable<TmAnswer> GetAnswers()
        {
            return _answers;
        }

        public void AddQuestionResult(int questionId, List<int> optionIds)
        {
            bool qResult = true;

            ITmQuestion question = _testForEvaluation.GetQuestions().First(q => q.Id == questionId);

            var answer = new TmAnswer();
            answer.ActualAnswers = optionIds;
            answer.Question = question;

            _answers.Add(answer);

            var corectOptions = question.GetOptions().Where(opt=>opt.IsCorrect == true);

            if(optionIds.Count() != corectOptions.Count() )
            {
                qResult = false;
                answerResults.Add(question.Id, qResult);
                return;
            }

            foreach (var option in corectOptions)
            {
                if(!optionIds.Contains(option.Id))
                {
                    qResult = false;
                    break;
                }
            }

            answerResults.Add(question.Id, qResult);

        }

        public float GetScore()
        {

            var correctAnswerCount = answerResults.Values.Count(result => result);
            return  (float)answerResults.Values.Count(result => result) / answerResults.Values.Count;

        }
    }
}
