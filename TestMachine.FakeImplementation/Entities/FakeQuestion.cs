﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;

namespace TestMachine.FakeImplementation.Entities
{
    public class FakeQuestion : ITmQuestion
    {
      

        private TmQuestionType _questionType;

        private List<ITmQuestionOption> _options = new List<ITmQuestionOption>();

        public FakeQuestion(TmQuestionType questionType)
        {
            _questionType = questionType;
        }

        public int Id { get; set; }

        public ITmTest Test { get; }

        public TmQuestionType QuestionType { get { return _questionType; } }

        public string Text { get; set; }

        public void AddOption(ITmQuestionOption option)
        {
            option.Id = _options.Count + 1;
            _options.Add(option);
        }

        public IEnumerable<ITmQuestionOption> GetOptions()
        {
            return _options;
        }
    }
}
