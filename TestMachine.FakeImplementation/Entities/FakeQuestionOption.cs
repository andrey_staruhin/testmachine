﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.FakeImplementation.Entities
{
    public class FakeQuestionOption : ITmQuestionOption
    {

        public FakeQuestionOption(string text, bool isCorrect)
        {
            Text = text;
            IsCorrect = isCorrect;
        }

        public int Id { set; get; }

        public string Text { set; get; }

        public bool IsCorrect { private set; get; }
    }
}
