﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Repositories;

namespace TestMachine.FakeImplementation.Repositories
{
    public class FakeTestTesultRepository : ITestResultRepository
    {

        static List<ITmTestResult> _testResults = new List<ITmTestResult>(); 

        public ITmTestResult AddTestResult(ITmTestResult testResult)
        {
            testResult.Id = _testResults.Count + 1;
            _testResults.Add(testResult);
            return testResult;
        }

        public IEnumerable<ITmTestResult> GetTestResults(object userId)
        {

          string strUserId = (string)userId;

          return  _testResults.Where(tr => (string)tr.UserId == strUserId);
        }
    }
}
