﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Repositories;

namespace TestMachine.FakeImplementation.Repositories
{
    public class FakeTmRepository : ITestRepository
    {
        public static List<ITmTest> _tests = new List<ITmTest>();


        public ITmTest AddTest(ITmTest test)
        {
            test.Id = _tests.Count + 1;
            _tests.Add(test);

            return test;
        }

        public IEnumerable<ITmTest> GetCompletedTestsForUser(object userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ITmTest> GetTests()
        {
            return _tests;
        }
    }
}
