﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;
using TestMachine.Domain.Factories;
using TestMachine.Domain.Repositories;
using TestMachine.FakeImplementation.Entities;
using TestMachine.FakeImplementation.Repositories;

namespace TestMachine.FakeImplementation.Factories
{
    public class FakeTmFactory : ITmFactory
    {
        public ITmQuestion CreateQuestion(TmQuestionType questionType, string text)
        {
            return new FakeQuestion(questionType) { Text = text };
        }

        public ITmQuestionOption CreateQuestionOption(string text, bool isCorrect)
        {
            return new FakeQuestionOption(text, isCorrect); 
        }

        public ITmTest CreateTest(string name)
        {
            return new FakeTest() {Name = name };
        }

        public ITmTestResult CreateTestResult(object userId, int testId)
        {
            return new FakeTestResult(GetTestRepository().GetTests().First(tst => tst.Id == testId)) { UserId = userId };
        }

        public ITestRepository GetTestRepository()
        {
            return new FakeTmRepository();
        }

        public ITestResultRepository GetTestResultRepository()
        {
            return new FakeTestTesultRepository();
        }
    }
}
