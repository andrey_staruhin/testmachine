﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMachine.WebUI.Models
{
    public class TestAnswersModel
    {

        public class QuestionAnswer {
           public int QuestionId { set; get; }

           public List<int> SelectedOptions { set; get; }
        }

        public List<QuestionAnswer> Answers { set; get; }

        public int TestId { set; get; }

        
    }
}
