﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;
using TestMachine.Domain.Factories;
using TestMachine.Domain.Repositories;
using TestMachine.FakeImplementation.Factories;
using TestMachine.Services;

namespace TestMachine.WebUI.Infrastructure
{
    public class Bindings
    {

        static bool fakeTestsCreated = false;

        private static ITmTest CreateFakeTest(string name)
        {
            TestCreationService testCreationService = CreateTestCreationService();
            ITmTest test1 = testCreationService.CreateTest(name);

            ITmQuestion question1_1 = testCreationService.CreateQuestion(TmQuestionType.SingleChoice, $"{name}_Question1");
            ITmQuestionOption option1 = testCreationService.CreateQuestionOption($"{name}_Option11", true);
            ITmQuestionOption option2 = testCreationService.CreateQuestionOption($"{name}_Option12", false);
            question1_1.AddOption(option1);
            question1_1.AddOption(option2);
            test1.AddQuestion(question1_1);

            ITmQuestion question1_2 = testCreationService.CreateQuestion(TmQuestionType.MultipleChoice, $"{name}_Question2");
            ITmQuestionOption option21 = testCreationService.CreateQuestionOption($"{name}_Option21", true);
            ITmQuestionOption option22 = testCreationService.CreateQuestionOption($"{name}_Option22", true);
            question1_2.AddOption(option21);
            question1_2.AddOption(option22);
            test1.AddQuestion(question1_2);

           return  testCreationService.AddTest(test1);
        }

        public static IEnumerable<ITmTest> GetFakeTests()
        {
            if (!fakeTestsCreated)
            {
                CreateFakeTest("Test1");
                CreateFakeTest("Test2");
                fakeTestsCreated = true;
            }

            return CreateTestCreationService().GetTests();
        }

        public static void CreateFakeData()
        {
            if (!fakeTestsCreated)
            {
                CreateFakeTest("Test1");
                CreateFakeTest("Test2");
                fakeTestsCreated = true;
            }
        }

        public static TestCreationService CreateTestCreationService()
        {
            return new TestCreationService(new FakeTmFactory());
        }
    }
}
