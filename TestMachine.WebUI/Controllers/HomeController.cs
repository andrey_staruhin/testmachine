﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Factories;
using TestMachine.Services;
using TestMachine.WebUI.Infrastructure;
using TestMachine.WebUI.Models;

namespace TestMachine.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private TestCreationService _testService;
        private readonly UserManager<ApplicationUser> _userManager;
        public HomeController(UserManager<ApplicationUser> userManager, ITmFactory tmFactory)
        {
            _testService = new TestCreationService(tmFactory);
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [Authorize]
        public IActionResult TestList()
        {


            return View();
        }



        public JsonResult GetTests()
        {
            // return Json(new { test = "test" });



            return Json(_testService.GetTests().Select(test => new { id = test.Id, name = test.Name }));
        }


        public JsonResult GetTestQuestions(int testId)
        {

            return Json(_testService.GetTestById(testId).GetQuestions().Select(q =>
            { return new { id = q.Id, text = q.Text, questionType = q.QuestionType, options = q.GetOptions().Select(opt => new { id = opt.Id, text = opt.Text }) }; }));
        }


        public JsonResult GetTestQuestionsWithAnswers(int testResultId)
        {

            var user = _userManager.GetUserAsync(User).Result;

            var testResult = _testService.GetTestResults(user.Id).First(tr => tr.Id == testResultId);

            var answers = testResult.GetAnswers().ToList();

            Func<int, int, bool> userTestOptionAnswer = (questionId, optId) => answers.First(answ => answ.Question.Id == questionId).ActualAnswers.Contains(optId);

            return Json(_testService.GetTestById(testResult.TestId).GetQuestions().Select(q =>
            { return new { id = q.Id, text = q.Text, questionType = q.QuestionType, options = q.GetOptions()
                .Select(opt => new { id = opt.Id, text = opt.Text, isCorrect = opt.IsCorrect, userVersion = userTestOptionAnswer(q.Id, opt.Id) }) }; }));
        }


        public JsonResult EvaluateTestAnswers()
        {

            var answers = Request.Form["testAnswers"];

            TestAnswersModel testAnswers = JsonConvert.DeserializeObject<TestAnswersModel>(answers);

            var user = _userManager.GetUserAsync(User).Result;

            ITmTestResult testResult = _testService.InitializeTestResult(user.Id, testAnswers.TestId);

            foreach (var qa in testAnswers.Answers)
            {
                testResult.AddQuestionResult(qa.QuestionId, qa.SelectedOptions);
            }
            var insertedTestResult = _testService.AddTestResult(testResult);



            return Json(new { testResultId = insertedTestResult.Id });
        }

        public JsonResult GetCompletedTestsForUser(int userId)
        {
            var user = _userManager.GetUserAsync(User).Result;

            var testResults = _testService.GetTestResults(user.Id);

            return Json(testResults.Select(tr => new { name = _testService.GetTests().First(t => t.Id == tr.TestId).Name, Id = tr.Id }));

        }


        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
