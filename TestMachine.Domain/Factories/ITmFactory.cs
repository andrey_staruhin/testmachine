﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;
using TestMachine.Domain.Repositories;

namespace TestMachine.Domain.Factories
{
    public interface ITmFactory
    {
        ITmTest CreateTest(string name);

        ITmQuestion CreateQuestion(TmQuestionType questionType, string text );

        ITmQuestionOption CreateQuestionOption(string text, bool isCorrect);
        
    

        ITmTestResult CreateTestResult(object userId, int testId);


        ITestRepository GetTestRepository();

        ITestResultRepository GetTestResultRepository();

    }
}
