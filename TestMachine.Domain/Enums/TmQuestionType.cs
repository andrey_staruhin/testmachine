﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMachine.Domain.Enums
{
     public enum TmQuestionType
    {
        SingleChoice,
        MultipleChoice
    }
}
