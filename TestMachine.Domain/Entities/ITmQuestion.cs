﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Enums;

namespace TestMachine.Domain.Entities
{
    public interface ITmQuestion
    {

        int Id { set; get; }

        ITmTest Test { get; }

        TmQuestionType QuestionType { get; }

        void AddOption(ITmQuestionOption option);
        
        string Text { set; get; }

        IEnumerable<ITmQuestionOption> GetOptions();
    }
}
