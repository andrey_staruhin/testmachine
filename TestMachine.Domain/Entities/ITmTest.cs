﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMachine.Domain.Entities
{
    public interface ITmTest
    {
        int Id { set; get; }

        string Name { set; get; }

        IEnumerable<ITmQuestion> GetQuestions();

        void AddQuestion(ITmQuestion question);
    }
}
