﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMachine.Domain.Entities
{
    public class TmAnswer
    {
       public ITmQuestion Question { set; get; }

       public   IEnumerable<int> ActualAnswers { set; get; }
    }
}
