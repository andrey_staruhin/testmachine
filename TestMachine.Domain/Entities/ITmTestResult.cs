﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMachine.Domain.Entities
{
    public interface ITmTestResult
    {

        IEnumerable<TmAnswer> GetAnswers();

        int Id { set;  get; }

        void AddQuestionResult(int questiomId, List<int> optionIdList);

        float GetScore();

        object UserId { set; get; }

        int TestId { set; get; }
    }
}
