﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMachine.Domain.Entities
{
    public interface ITmQuestionOption
    {
        int Id { set; get; }

        string Text { set; get; }

        bool IsCorrect { get; }
    }
}
