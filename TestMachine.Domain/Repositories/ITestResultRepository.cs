﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.Domain.Repositories
{
    public interface ITestResultRepository
    {
        ITmTestResult AddTestResult(ITmTestResult testResult);

        IEnumerable<ITmTestResult> GetTestResults(object userId);
    }
}
