﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.Domain.Entities;

namespace TestMachine.Domain.Repositories
{
    public interface ITestRepository
    {
        IEnumerable<ITmTest> GetCompletedTestsForUser(object userId);

        IEnumerable<ITmTest> GetTests();

        ITmTest AddTest(ITmTest test);

    }
}
