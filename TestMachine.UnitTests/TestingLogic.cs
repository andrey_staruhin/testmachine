using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;
using TestMachine.Services;

namespace TestMachine.UnitTests
{
    [TestClass]
    public class TestingLogic
    {
      

        [TestMethod]
        public void CreateTestWithQuestion()
        {
            TestCreationService testCreationService = Bindings.CreateTestCreationService();

            ITmTest test1 = testCreationService.CreateTest("New Test");

            Assert.AreEqual("New Test", test1.Name);

            ITmQuestion question = testCreationService.CreateQuestion(TmQuestionType.SingleChoice, "Question1");

            Assert.AreEqual(question.QuestionType, TmQuestionType.SingleChoice);

            test1.AddQuestion(question);

            Assert.AreEqual(test1.GetQuestions().Count(), 1);
            Assert.AreEqual(test1.GetQuestions().ToList()[0].Text, "Question1");
        }



        public void AddQuestionToTest()
        {
            


        }

        [TestMethod]
        public void AddOptionsToQuestion()
        {
            TestCreationService testCreationService = Bindings.CreateTestCreationService();
            ITmTest test1 = testCreationService.CreateTest("New Test");

            ITmQuestion question = testCreationService.CreateQuestion(TmQuestionType.SingleChoice, "Question1");

            ITmQuestionOption option1 = testCreationService.CreateQuestionOption("Option1", true);
            ITmQuestionOption option2 = testCreationService.CreateQuestionOption("Option2", false);

            question.AddOption(option1);
            question.AddOption(option2);

            test1.AddQuestion(question);

            List<ITmQuestion> questionList =  test1.GetQuestions().ToList();

            Assert.AreEqual(questionList[0].GetOptions().Count(), 2);
            Assert.AreEqual(questionList[0].GetOptions().ToList()[0].Text, "Option1");
            Assert.AreEqual(questionList[0].GetOptions().ToList()[1].Text, "Option2");


        }


       

        private ITmTest CreateFakeTest(string name, params string[] questionNames)
        {
            TestCreationService testCreationService = Bindings.CreateTestCreationService();
            ITmTest test1 = testCreationService.CreateTest(name);

            foreach( string qName in questionNames)
            {
                ITmQuestion question = testCreationService.CreateQuestion(TmQuestionType.SingleChoice, qName);

                ITmQuestionOption option1 = testCreationService.CreateQuestionOption($"{qName}_Option1", true);
                ITmQuestionOption option2 = testCreationService.CreateQuestionOption($"{qName}_Option2", false);

                question.AddOption(option1);
                question.AddOption(option2);

                test1.AddQuestion(question);
            }

            return test1;
        }

        [TestMethod]
        public void SaveTestsAndGetTestById()
        {
            ITmTest test1 = CreateFakeTest("Test1", "Q11", "Q12");
            ITmTest test2 = CreateFakeTest("Test2", "Q21", "Q22");

           TestCreationService testCreationService = Bindings.CreateTestCreationService();

            ITmTest insertedTest1 =  testCreationService.AddTest(test1);
            ITmTest insertedTest2 = testCreationService.AddTest(test2);


            TestCreationService testCreationService2 = Bindings.CreateTestCreationService();

            List<ITmTest> createdTests = testCreationService2.GetTests().ToList();

            Assert.AreEqual(createdTests.Count(), 2);

            ITmTest foundTest1 = testCreationService2.GetTestById(insertedTest1.Id);
            ITmTest foundTest2 = testCreationService2.GetTestById(insertedTest2.Id);

            Assert.AreEqual(foundTest1.Name, "Test1");
         


            Assert.AreEqual(foundTest2.Name, "Test2");

             
        }



        [TestMethod]
        public void EvaluateUserTest()
        {
            TestCreationService testCreationService = Bindings.CreateTestCreationService();
            ITmTest test1 = testCreationService.CreateTest("New Test");

            ITmQuestion question = testCreationService.CreateQuestion(TmQuestionType.SingleChoice, "Question1");
            ITmQuestionOption option1 = testCreationService.CreateQuestionOption("Option1", true);
            ITmQuestionOption option2 = testCreationService.CreateQuestionOption("Option2", false);
            question.AddOption(option1);
            question.AddOption(option2);
            test1.AddQuestion(question);
           
            ITmQuestion question2 = testCreationService.CreateQuestion(TmQuestionType.MultipleChoice, "Question2");
            ITmQuestionOption option21 = testCreationService.CreateQuestionOption("Option21", true);
            ITmQuestionOption option22 = testCreationService.CreateQuestionOption("Option22", true);
            question2.AddOption(option21);
            question2.AddOption(option22);
            test1.AddQuestion(question2);

            testCreationService.AddTest(test1);

            int userId = 1;
            ITmTestResult testResult = testCreationService.InitializeTestResult(userId, test1.Id);

            List<int> answers1 = new List<int> { option1.Id };
            testResult.AddQuestionResult(question.Id, answers1);

            List<int> answers2 = new List<int> { option21.Id };

            testResult.AddQuestionResult(question2.Id, answers2);

            Assert.AreEqual(testResult.GetScore(), 0.5);

            List<TmAnswer> answerResults = testResult.GetAnswers().ToList();

            TmAnswer answer = answerResults.First(answ => answ.Question.Id == question.Id);

            Assert.AreEqual(answer.ActualAnswers.Where(opt=>answers1.Contains(opt)).Count(), answers1.Count);

        }



        public void GenerateTestForUser()
        {

        }


        [TestMethod]
        public void GetTestingHistory()
        {
            TestCreationService testCreationService = Bindings.CreateTestCreationService();
            ITmTest test1 = testCreationService.CreateTest("New Test");

            ITmQuestion question1_1 = testCreationService.CreateQuestion(TmQuestionType.SingleChoice, "Question1");
            ITmQuestionOption option1 = testCreationService.CreateQuestionOption("Option11", true);
            ITmQuestionOption option2 = testCreationService.CreateQuestionOption("Option12", false);
            question1_1.AddOption(option1);
            question1_1.AddOption(option2);
            test1.AddQuestion(question1_1);

            ITmQuestion question1_2 = testCreationService.CreateQuestion(TmQuestionType.MultipleChoice, "Question2");
            ITmQuestionOption option21 = testCreationService.CreateQuestionOption("Option21", true);
            ITmQuestionOption option22 = testCreationService.CreateQuestionOption("Option22", true);
            question1_2.AddOption(option21);
            question1_2.AddOption(option22);
            test1.AddQuestion(question1_2);

            testCreationService.AddTest(test1);

            int userId = 1;
            ITmTestResult testResult = testCreationService.InitializeTestResult(userId, test1.Id);

            List<int> answers1 = new List<int> { option1.Id };
            testResult.AddQuestionResult(question1_1.Id, answers1);

            List<int> answers2 = new List<int> { option21.Id };

            testResult.AddQuestionResult(question1_2.Id, answers2);


            testCreationService.AddTestResult(testResult);

            TestCreationService testCreationService2 = Bindings.CreateTestCreationService();

            IEnumerable<ITmTestResult> testResults = testCreationService2.GetTestResults(userId);


            Assert.AreEqual(testResults.Count(), 1);


            var answerResults = testResults.ToList()[0].GetAnswers();

            TmAnswer answer = answerResults.First(answ => answ.Question.Id == question1_1.Id);

            Assert.AreEqual(answer.ActualAnswers.Where(opt => answers1.Contains(opt)).Count(), answers1.Count);
        }



    }
}
