﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMachine.FakeImplementation.Factories;
using TestMachine.Services;

namespace TestMachine.UnitTests
{
    public class Bindings
    {
        public static TestCreationService CreateTestCreationService()
        {
            return new TestCreationService(new FakeTmFactory());
        }
    }
}
