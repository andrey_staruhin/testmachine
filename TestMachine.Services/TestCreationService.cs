﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMachine.Domain.Entities;
using TestMachine.Domain.Enums;
using TestMachine.Domain.Factories;
using TestMachine.Domain.Repositories;

namespace TestMachine.Services
{
    public class TestCreationService
    {
        ITmFactory _tmFactory;

        public TestCreationService(ITmFactory tmFactory)
        {
            _tmFactory = tmFactory;
        }

        public ITmTest CreateTest(string name)
        {
            return _tmFactory.CreateTest(name);
        }


        public ITmQuestion CreateQuestion(TmQuestionType questionType, string text)
        {
            return _tmFactory.CreateQuestion(questionType, text);
        }

        public ITmQuestionOption CreateQuestionOption(string text, bool isCorrect)
        {
            return _tmFactory.CreateQuestionOption(text, isCorrect);
        }

        public ITmTest AddTest(ITmTest test)
        {
          return  _tmFactory.GetTestRepository().AddTest(test);
        }

        public IEnumerable<ITmTest> GetTests()
        {
            return _tmFactory.GetTestRepository().GetTests();
        }


        public ITmTest GetTestById(int id)
        {
            return GetTests().First(test => test.Id == id);
        }


        public ITmTestResult InitializeTestResult(object userId, int testId)
        {
            return _tmFactory.CreateTestResult(userId, testId);
        }


        public ITmTestResult AddTestResult(ITmTestResult testResult)
        {
          return  _tmFactory.GetTestResultRepository().AddTestResult(testResult);
        }

        public IEnumerable<ITmTestResult> GetTestResults(object userId)
        {
            return _tmFactory.GetTestResultRepository().GetTestResults(userId);
        }
    }
}
